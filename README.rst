ACRCms on Red Hat's OpenShift PaaS
==========================================

This quickstart helps you get up and running with a `ACRCms <http://www.acrcms.org/>`_  instance on OpenShift. 
It automatically handles creating a Python virtualenv, populating a MySQL database,
and deploying the cms to the cloud.

ACRCms
-------

ACRCms is an open source Python
web content management system based on
Turbogears and libacr.
ACRCms is designed to let non technical people create and manage their web pages
and thanks to its plugin system and libacr it is also easily extensible
and embeddable in any web site using the Turbogears framework.

Features
--------

* Completely free, thanks to Red Hat's OpenShift Express
* MySQL database automatically setup
* Dynamic database configuration at runtime. No passwords stored in your configs.
* Automatic deployment upon git push
* No need to think about servers, let alone apache/mod_wsgi configuration

How To
-----------------

* Create an account at http://openshift.redhat.com/
* Add a namespace to your account:

::

    rhc domain create -n <yournamespace> -l your@email.com

* Deploy the cms:

::

    rhc app create -a ACRCms -t python-2.6 -l your@email.com
    rhc app cartridge add -a ACRCms -c mysql-5.1 -l your@email.com
    cd ACRCms
    git remote add upstream -m master git@bitbucket.org:simock85/acrcms-openshift-quickstart.git
    git pull -s recursive -X theirs upstream master
    git push

Monitoring your logs
--------------------

::

    rhc-tail-files -a ACRCms -l your@email.com
