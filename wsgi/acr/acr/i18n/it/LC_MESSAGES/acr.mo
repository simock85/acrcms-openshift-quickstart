��    F      L              |     }     �     �  	   �     �     �     �     �     �     �  
   �  	   �       	             )     <     I     ^     m     {     �  	   �  
   �  }   �     #      /     P     V     ]     e  "   r     �     �     �     �  
   �     �  	   �     �  	   �  
   �     �                    *     9     >     R  >   c     �     �     �     �     �     �     �       !        @     M  	   Y     c     h     v  
   �     �     �  �  �     2
     9
  	   N
  
   X
     c
     s
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
          !     8     N  
   c  	   n  
   x     �  �   �     %  "   2     U     \     d     m     {  	   �     �  	   �     �     �     �     �     �     �            
   %     0     F     V     j     r     �  L   �     �     �     �     �          $  "   D     g      �     �     �     �     �     �     �                  Actions Add To Cart Cart: Category: Confirm Order Confirm Payment Dispatched Orders Edit Email Featured Products First Name In Stock: Info of Order Last Name Manage Orders Mark as Dispatched Mark as Paid Mark as Undispatched Mark as Unpaid Order Content Order From: Order ID Order ID: Order Info Order registered but not payed, Please provide your order id either by telephone or fax to proceed with payment. Order ID: %s Orders List Payment successfully registered. Price Price: Product Product Name Product is currently not available Product: Products Quantity Search Search for Ship Address Ship City Ship Country Ship Name Ship State Ship Zip Ship to: Shipping Days: Shipping Name Shipping Time: Shop Stroller Management Stroller Manager There has been an error in processing your payment request: %s Total Total Price Total: Undispatched Orders Unpaid Orders Users' favourite products Your Cart was reset Your cart is empty Your transaction has been revoked add category add product available days days shipping delete this category in stock : items, shipping in Project-Id-Version: stroller 0.1
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2011-03-08 12:01+0100
PO-Revision-Date: 2011-03-09 11:07+0100
Last-Translator: Mirko Darino <mirko.darino@axant.it>
Language-Team: it <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 0.9.5
 Azioni Aggiungi Al Carrello Carrello: Categoria: Conferma Ordine Conferma Pagamento Ordini Spediti Modifica Email Prodotti in vetrina Nome In Magazzino: Informazioni sull'ordine Cognome Gestisci Ordini Segna come Spedito Segna come Pagato Segna come Non Spedito Segna come Non Pagato Ordine dei contenuti Ordine Da: ID Ordine ID Ordine: Informazione Ordine Ordine registrato ma non pagato, Vi preghiamo di fornire il vostro ID ordine per telefono o fax per procedere con il pagamento. ID ordine: %s Lista Ordini Pagamento registrato con successo. Prezzo Prezzo: Prodotto Nome Prodotto Prodotto non disponibile Prodotto: Prodotti Quantità Cerca Cerca Indirizzo Spedizione Città Spedizione Paese Spedizione Nome Spedizione Stato Spedizione CAP Spedizione Spedire a: Giorni di spedizione: Nome Spedizione Tempo di Trasporto: Negozio Gestione Stroller Gestore Stroller C'è stato un errore nella elaborazione della tua richiesta di pagamento: %s Totale Prezzo Totale Totale: Ordini Non Spediti Ordini Non Pagati Prodotti preferiti dagli utenti Il tuo carrello è stato resettato Il tuo carrello è vuoto La transazione è stata revocata aggiungi categoria aggiungi prodotto disponibile giorni giorno di spedizione elimina questa categoria in magazzino: oggetti, spedizione in 