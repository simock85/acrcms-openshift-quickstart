# -*- coding: utf-8 -*-

"""WebHelpers used in acr."""
from webhelpers import date, feedgenerator, html, number, misc, text
import libacr
from libacr.helpers import *

icons = {}
icons.update(libacr.helpers.icons)
