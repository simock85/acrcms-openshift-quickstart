from libacr.model.core import DBSession
from libacr.views.base import EncodedView
from libacr.views.manager import ViewsManager
from smallpress.model import Blog, Article
from repoze.what import predicates
from tg import config, TGController, request, redirect, url
from libacr.plugins.base import AcrPlugin, AdminEntry
from tg.decorators import expose, require
from tgext.pluggable.utils import call_partial
import tw.forms as twf
import acr.model
import os

class SmallPressPluginController(TGController):
    @expose()
    @require(predicates.in_any_group('acr', 'acr_editors'))
    def manage(self, *args, **kw):
        if 'smallpress' not in request.identity['groups']:
            g = DBSession.query(acr.model.Group).filter_by(group_name='smallpress').first()
            request.identity['user'].groups.append(g)
        return redirect('/smallpress/blogs')


class SmallPressArticlesRenderer(EncodedView):
    def __init__(self):
        self.name='blog_articles'
        self.form_fields=[twf.SingleSelectField('blog_id', label_text='Blog:',
            validator=twf.validators.Int(not_empty=True, strip=True),
            options=lambda: ((b.uid, b.name) for b in DBSession.query(Blog)))]
        self.exposed = True

    def preview(self, page, slice, data):
        data = self.to_dict(data)
        blog = DBSession.query(Blog).filter_by(uid=data['blog_id']).first()

        articles = list(Article.get_published(blog)[:1])
        if not articles:
            return ''

        article = articles[0]
        html = u'<div id="acr_blog_%s_articles_preview">' % slice.uid

        cover = [att for att in article.attachments if att.name == 'cover']
        if cover:
            html += u'<img src="%s"/>' % cover[0].content.url

        html += u'<h3>%s</h3>' % article.title
        html += u'<div class="acr_blog_articles_preview_date">%s</div>' % article.publish_date.strftime('%Y-%m-%d')
        html += u'<div class="acr_blog_articles_preview_description">%s</div>' % article.description
        html += u'</div>'

        return html


    def render(self, page, slice, data):
        data = self.to_dict(data)
        blog = DBSession.query(Blog).filter_by(uid=data['blog_id']).first()

        html = u''
        if request.identity and 'smallpress' in request.identity['groups']:
            html += u'''
<div class="acr_blog_admin">
    <a href="%s">Manage Blog</a>
</div>'''% url('/smallpress/manage', params={'blog':blog.name})

        html += u'<div id="acr_blog_%s_articles">' % slice.uid
        html += unicode(call_partial('smallpress.partials:articles', blog=blog))
        html += u'</div>'

        return html


class SmallPressPlugin(AcrPlugin):
    uri = "blogs"
    last_version = 1

    def upgrade(self, DBSession, current_version):
        if current_version < 1:
            from smallpress.model import Article, Attachment, Blog

        if not Blog.__table__.exists(bind=DBSession.bind):
            acr.model.metadata.create_all(bind=DBSession.bind)

        if not DBSession.query(acr.model.Group).filter_by(group_name='smallpress').first():
            DBSession.add(acr.model.Group(group_name='smallpress', display_name='Smallpress Users'))

        attachments_path = config.get('attachments_path')
        if not attachments_path:
            attachments_path = os.path.join(config['here'],
                config['package'].__name__.lower(),
                'public', 'attachments')

        if not os.path.exists(attachments_path):
            os.makedirs(attachments_path)

    def __init__(self):
        self.admin_entries = [AdminEntry(self, 'Manage Blogs', 'manage', section="Templates", icon="blog_compose.png")]
        self.controller = SmallPressPluginController()
        ViewsManager.register_view(SmallPressArticlesRenderer())