from tg import expose, flash, require, request, redirect, tmpl_context, TGController, config, validate, url
from tg.controllers import WSGIAppController
import libacr, os

import acr.model
from libacr.model.core import DBSession
from photos.model import Gallery
from libacr.controllers.admin.base import BaseAdminController
from libacr.plugins.base import AdminEntry, AcrPlugin, plugin_expose
from pylons.controllers.util import abort
from repoze.what import predicates
from tw.api import WidgetsList
import tw.forms as twf
from libacr.views.manager import ViewsManager
from libacr.views.base import EncodedView
from sqlalchemy.exc import OperationalError
from tgext.pluggable import call_partial
from tw.api import CSSLink, JSLink

class PhotosPluginController(TGController):
    @expose()
    @require(predicates.in_any_group('acr', 'acr_editors'))
    def manage(self, *args, **kw):
        if 'photos' not in request.identity['groups']:
            g = DBSession.query(acr.model.Group).filter_by(group_name='photos').first()
            request.identity['user'].groups.append(g)
        return redirect('/photos/manage/galleries')

class PhotosAlbumViewRenderer(EncodedView):
    def __init__(self):
        self.name = 'photo_album'
        self.form_fields = [twf.SingleSelectField('gallery_id', label_text="Album:",
            validator=twf.validators.Int(not_empty=True, strip=True),
            options=lambda : ((p.uid, p.name) for p in DBSession.query(Gallery)))]
        self.exposed = True

    def preview(self, page, slice, data):
        return 'Preview not Implemented'

    def render(self, page, slice, data):
        data = self.to_dict(data)
        gallery = DBSession.query(Gallery).filter_by(uid=data['gallery_id']).first()

        html = u''
        if request.identity and 'acr' in request.identity['groups']:
            html += u'''
<div class="acr_album_admin">
    <a href="%s">Manage Photos</a>
</div>
''' % url('/photos/manage/photos/', params=dict(gallery=data['gallery_id']))

        html += '<div id="acr_albumns_%s">' % slice.uid
        html += unicode(call_partial('photos.partials:gallery', gallery=gallery))
        html += '</div>'

        html += '''
<script>
        jQuery('#acr_albumns_%s a').lightBox({
            'imageBtnPrev':'/_pluggable/photos/images/lightbox-btn-prev.gif',
            'imageBtnNext':'/_pluggable/photos/images/lightbox-btn-next.gif',
            'imageBtnClose':'/_pluggable/photos/images/lightbox-btn-close.gif',
            'imageLoading':'/_pluggable/photos/images/lightbox-ico-loading.gif'
        });
    </script>
''' % slice.uid

        return html

class PhotosPlugin(AcrPlugin):
    uri = 'photos'
    last_version = 1

    def upgrade(self, DBSession, current_version):
        if current_version < 1:
            from photos.model import Gallery, Photo

        if not Gallery.__table__.exists(bind=DBSession.bind):
            acr.model.metadata.create_all(bind=DBSession.bind)

        if not DBSession.query(acr.model.Group).filter_by(group_name='photos').first():
            DBSession.add(acr.model.Group(group_name='photos', display_name='Photos users'))

        attachments_path = config.get('attachments_path')
        if not attachments_path:
            attachments_path = os.path.join(config['here'],
                                            config['package'].__name__.lower(),
                                            'public', 'attachments')

        if not os.path.exists(attachments_path):
            os.makedirs(attachments_path)

    def __init__(self):
        self.admin_entries = [AdminEntry(self, 'Manage Photo Albums', 'manage', section="Templates", icon="photo_album.png")]
        self.css_resources = [CSSLink(link='/_pluggable/photos/css/jquery.lightbox-0.5.css')]
        self.js_resources = [JSLink(link='/_pluggable/photos/js/jquery.lightbox-0.5.min.js')]
        self.controller = PhotosPluginController()
        ViewsManager.register_view(PhotosAlbumViewRenderer())