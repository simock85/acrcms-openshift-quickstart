from formencode import validators
from genshi.core import Markup
from libacr.controllers.admin.base import BaseAdminController
from libacr.lib import url
from libacr.plugins.base import plugin_expose, AcrPlugin, AdminEntry
from repoze.what import predicates
from tg.decorators import require, expose, validate
from tg.flash import flash
from tw.core.meta import WidgetsList
from tw.forms.fields import TableForm, TextField, SingleSelectField
from acr.model import DBSession
from acr.model.auth import User, Group
from tg import redirect

__author__ = 'simock85'

class NewUserForm(TableForm):
    class fields(WidgetsList):
        username = TextField(label_text='User Name', validator=validators.String())
        email = TextField(label_text='Email', validators=validators.Email())
        group_name = SingleSelectField(label_text="Role",
            options=[('acr', 'Web Site Manager'), ('acr_editors', 'Content Editor'), ('', 'Generic User')],
            validators=validators.String(), default='acr_editors')
new_user_form = NewUserForm()

class ManageUserController(BaseAdminController):
    @plugin_expose('index')
    @require(predicates.in_group('acr'))
    def index(self):
        users_dict = []
        users = DBSession.query(User)
        for user in users:
            u = {}
            u['user_id'] = user.user_id
            u['user_name'] = user.user_name
            u['email_address'] = user.email_address
            u['password_edit'] = self.check_password_edit(user.user_id)
            u['acr_role'] = self.check_acr_user(user.user_id)
            users_dict.append(u)
        return dict(form=new_user_form, users=users_dict)

    def check_password_edit(self, user):
        user = DBSession.query(User).get(user)
        return 'NO!' if user.validate_password(user.user_name) else 'YES'

    @expose()
    @validate({'user': validators.Int()})
    def check_acr_user(self, user):
        user = DBSession.query(User).get(user)
        groups = [g.group_name for g in user.groups if g.group_name in ('acr', 'acr_editors')]
        if groups:
            return 'Web Site Manager' if 'acr' in groups else 'Content Editor'
        return Markup('<a href="%s">Make Editor</a>' % url('/plugins/manage_users/make_editor', user=user.user_id))

    @expose()
    @validate({'user': validators.Int()})
    @require(predicates.in_group('acr'))
    def make_editor(self, user):
        user = DBSession.query(User).get(user)
        group = DBSession.query(Group).filter(Group.group_name=='acr_editors').first()

        if group is None:
            group = ManageUserController.create_editors_group()

        group.users.append(user)
        return redirect(url('/plugins/manage_users'))

    @expose()
    @validate(form=new_user_form, error_handler=index)
    @require(predicates.in_group('acr'))
    def submit(self, username, email, group_name):
        user = User(user_name=username, password=username, display_name=username, email_address=email)
        if not group_name:
            flash('User Created')
            return redirect(url('/plugins/manage_users'))

        group = DBSession.query(Group).filter(Group.group_name==group_name).first()

        if group is None and group_name=='acr_editors':
            group = ManageUserController.create_editors_group()

        group.users.append(user)
        return redirect(url('/plugins/manage_users'))

    @expose()
    @validate({'user': validators.Int()})
    def delete(self, user):
        #avoid to delete the first user
        if user is 1:
            return redirect(url('/plugins/manage_users'))
        user = DBSession.query(User).get(user)
        DBSession.delete(user)
        return redirect(url('/plugins/manage_users'))

    @staticmethod
    def create_editors_group():
        group = Group(group_name='acr_editors', display_name='ACR Editors')
        DBSession.add(group)
        DBSession.flush()
        return group

class ManageUsers(AcrPlugin):
    uri = 'manage_users'

    def __init__(self):
        self.admin_entries = [AdminEntry(self, 'Manage Users', 'index', icon='Gnome-System-Users-64.png', section="General Settings")]
        self.controller = ManageUserController()



